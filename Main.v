`timescale 1ns / 1ps
module Main(
	input clk,
	output reg [2:0]vga_r,
	output reg [2:0]vga_g,
	output reg [1:0]vga_b,
	output reg vsync,
	output reg hsync
);

wire vga_clk;
reg [9:0] hcount, vcount;
reg [8:0] count;

DCM_SP #(.CLKFX_DIVIDE(8), .CLKFX_MULTIPLY(2), .CLKIN_PERIOD(10)) 
	vga_clock_dcm (.CLKIN(clk), .CLKFX(vga_clk), .CLKFB(1'd0), .PSEN(1'd0), .RST(1'd0));

parameter sqrHalf = 10'd10;
parameter iniPosX = 320;
parameter iniPosY = 240;

reg [9:0] posX = iniPosX, posY = iniPosY;
reg [9:0]leftPos, rightPos, upPos, downPos;

reg up = 1'd0, down = 1'd1, left = 1'd0, right = 1'd1;

reg [2:0]sqr_r = 3'd7, sqr_g = 3'd0, bkg_r = 3'd7, bkg_g = 3'd7;
reg [1:0]sqr_b = 2'd0, bkg_b = 2'd3;

always @(posedge vga_clk) begin
	leftPos = posX-sqrHalf;
	rightPos = posX+sqrHalf;
	upPos = posY-sqrHalf;
	downPos = posY+sqrHalf;
	
	if(hcount == 799) begin
		hcount = 0;
		if(vcount == 524) vcount = 0;
		else vcount = vcount + 1'd1;
	end
	else hcount = hcount + 1'd1;
	
	if(vcount >= 490 && vcount < 492) {vsync, count} = {1'd0, count + 8'd1};
	else vsync = 1;
	
	if(hcount >= 656 && hcount < 752) hsync = 0;
	else hsync = 1;
	
	if (hcount<640 && vcount<480) begin
		if(hcount > leftPos && hcount < rightPos
		&& vcount > upPos && vcount < downPos) begin
			vga_r = sqr_r;
			vga_g = sqr_g;
			vga_b = sqr_b;
		end
		else begin
			vga_r = bkg_r;
			vga_g = bkg_g;
			vga_b = bkg_b;
		end
	end
	else begin
		vga_r = 3'd0;
		vga_g = 3'd0;
		vga_b = 2'd0;
	end
end

always @(posedge vga_clk) begin
	if(count == 8'd255) begin
		if(right) posX <= posX + 10'd1;
		if(left) posX <= posX - 10'd1;
		if(down) posY <= posY +10'd1;
		if(up) posY <= posY - 10'd1;
	end
end

always @(posedge vga_clk) begin
	if(rightPos >= 640) begin
		{right, left} <= {1'd0, 1'd1};
		sqr_r <= 3'd7; bkg_r <= 3'd7;
		sqr_g <= 3'd0; bkg_g <= 3'd7;
		sqr_b <= 2'd0; bkg_b <= 2'd3;
	end
	if(leftPos <= 0) begin
		{right, left} <= {1'd1, 1'd0};
		sqr_r <= 3'd0; bkg_r <= 3'd0;
		sqr_g <= 3'd7; bkg_g <= 3'd0;
		sqr_b <= 2'd0; bkg_b <= 2'd3;
	end
	if(downPos >= 480) begin
		{up, down} <= {1'd1, 1'd0};
		sqr_r <= 3'd0; bkg_r <= 3'd7;
		sqr_g <= 3'd0; bkg_g <= 3'd5;
		sqr_b <= 2'd3; bkg_b <= 2'd2;
	end
	if(upPos <= 0) begin
		{up, down} <= {1'd0, 1'd1};
		sqr_r <= 3'd7; bkg_r <= 3'd0;
		sqr_g <= 3'd0; bkg_g <= 3'd0;
		sqr_b <= 2'd3; bkg_b <= 2'd0;
	end
end

endmodule
